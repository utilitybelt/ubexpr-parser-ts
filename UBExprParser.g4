parser grammar UBExprParser;

options {
    tokenVocab = UBExprLexer;
}

root: (WHITESPACE* expression (WHITESPACE* SEMICOLON WHITESPACE* expression)* WHITESPACE* SEMICOLON?) WHITESPACE* EOF;

expression
    : OPENPAREN functionArgs CLOSEPAREN WHITESPACE* FATARROW WHITESPACE* OPENCURL ( functionBody | ) CLOSECURL #inlineFunction
    | expression OPENPAREN userFunctionCallArgs? CLOSEPAREN                                      #userFunctionCallExp
    | OPENPAREN WHITESPACE* expression WHITESPACE* CLOSEPAREN                                    #parenthesisExp
    | id=( DOLLAR | AT | AMPER ) expression                                                      #getvarAtomExp
    | expression (OPENCURL (c=COLON | (c=COLON i2=expression) | i1=expression | (i1=expression c=COLON i2=expression) | (i1=expression c=COLON)) CLOSEPAREN)    #getindexAtomExp
    | (MINUS)? NUMBER                                                                            #numericAtomExp
    | USTRING expressionList                                                                     #functionCall
    | '~' expression                                                                             #bitwiseComplementOp
    | expression WHITESPACE* op=( OPBITSHIFTR | OPBITSHIFTL ) WHITESPACE* expression             #bitshiftOps
    | expression WHITESPACE* op=( AMPER | CARAT | BAR ) WHITESPACE* expression                   #bitwiseOps
    | <assoc=right> expression WHITESPACE* CARAT WHITESPACE* expression                          #powerExp
    | expression WHITESPACE* op=( ASTERISK | FSLASH | PERCENT ) WHITESPACE*expression            #mulDivExp
    | expression WHITESPACE* op=( PLUS | MINUS ) WHITESPACE* expression                          #addSubExp
    | expression POUND expression                                                                #regexExp
    | id=( DOLLAR | AT | AMPER ) expression WHITESPACE* EQ WHITESPACE* expression                #setVarExp
    | expression WHITESPACE* op=( GT | LT | GTEQ | LTEQ | EQEQ | NEQ ) WHITESPACE* expression    #comparisonExp
    | expression WHITESPACE* op=( OPAND | OPOR ) WHITESPACE* expression                          #booleanComparisonExp
    | BOOL                                                                                       #boolAtomExp
    | str=(STRING | USTRING)                                                                     #stringAtomExp
    | HEX                                                                                        #hexNumberAtomExp
    | .                                                                                          #catchallAtomExp
    ;

expressionList: OPENSQUARE WHITESPACE* (expression WHITESPACE* (COMMA WHITESPACE* expression)*)? WHITESPACE* CLOSESQUARE ;
functionBody: (WHITESPACE* expression (WHITESPACE* SEMICOLON WHITESPACE* expression)* WHITESPACE* SEMICOLON?) WHITESPACE* ;
userFunctionCallArgs: expression WHITESPACE* (COMMA WHITESPACE* expression)* ;

functionArgs: (USTRING WHITESPACE* (COMMA WHITESPACE* USTRING)*)? ;