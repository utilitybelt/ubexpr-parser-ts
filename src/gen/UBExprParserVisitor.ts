// Generated from ./UBExprParser.g4 by ANTLR 4.13.1

import { AbstractParseTreeVisitor } from "antlr4ng";


import { RootContext } from "./UBExprParser.js";
import { BitshiftOpsContext } from "./UBExprParser.js";
import { NumericAtomExpContext } from "./UBExprParser.js";
import { CatchallAtomExpContext } from "./UBExprParser.js";
import { BitwiseComplementOpContext } from "./UBExprParser.js";
import { MulDivExpContext } from "./UBExprParser.js";
import { BoolAtomExpContext } from "./UBExprParser.js";
import { GetvarAtomExpContext } from "./UBExprParser.js";
import { InlineFunctionContext } from "./UBExprParser.js";
import { UserFunctionCallExpContext } from "./UBExprParser.js";
import { SetVarExpContext } from "./UBExprParser.js";
import { GetindexAtomExpContext } from "./UBExprParser.js";
import { BitwiseOpsContext } from "./UBExprParser.js";
import { AddSubExpContext } from "./UBExprParser.js";
import { BooleanComparisonExpContext } from "./UBExprParser.js";
import { StringAtomExpContext } from "./UBExprParser.js";
import { RegexExpContext } from "./UBExprParser.js";
import { PowerExpContext } from "./UBExprParser.js";
import { ComparisonExpContext } from "./UBExprParser.js";
import { FunctionCallContext } from "./UBExprParser.js";
import { HexNumberAtomExpContext } from "./UBExprParser.js";
import { ParenthesisExpContext } from "./UBExprParser.js";
import { ExpressionListContext } from "./UBExprParser.js";
import { FunctionBodyContext } from "./UBExprParser.js";
import { UserFunctionCallArgsContext } from "./UBExprParser.js";
import { FunctionArgsContext } from "./UBExprParser.js";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `UBExprParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export class UBExprParserVisitor<Result> extends AbstractParseTreeVisitor<Result> {
    /**
     * Visit a parse tree produced by `UBExprParser.root`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRoot?: (ctx: RootContext) => Result;
    /**
     * Visit a parse tree produced by the `bitshiftOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBitshiftOps?: (ctx: BitshiftOpsContext) => Result;
    /**
     * Visit a parse tree produced by the `numericAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitNumericAtomExp?: (ctx: NumericAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `catchallAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitCatchallAtomExp?: (ctx: CatchallAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `bitwiseComplementOp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBitwiseComplementOp?: (ctx: BitwiseComplementOpContext) => Result;
    /**
     * Visit a parse tree produced by the `mulDivExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMulDivExp?: (ctx: MulDivExpContext) => Result;
    /**
     * Visit a parse tree produced by the `boolAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBoolAtomExp?: (ctx: BoolAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `getvarAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGetvarAtomExp?: (ctx: GetvarAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `inlineFunction`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitInlineFunction?: (ctx: InlineFunctionContext) => Result;
    /**
     * Visit a parse tree produced by the `userFunctionCallExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitUserFunctionCallExp?: (ctx: UserFunctionCallExpContext) => Result;
    /**
     * Visit a parse tree produced by the `setVarExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitSetVarExp?: (ctx: SetVarExpContext) => Result;
    /**
     * Visit a parse tree produced by the `getindexAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitGetindexAtomExp?: (ctx: GetindexAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `bitwiseOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBitwiseOps?: (ctx: BitwiseOpsContext) => Result;
    /**
     * Visit a parse tree produced by the `addSubExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitAddSubExp?: (ctx: AddSubExpContext) => Result;
    /**
     * Visit a parse tree produced by the `booleanComparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBooleanComparisonExp?: (ctx: BooleanComparisonExpContext) => Result;
    /**
     * Visit a parse tree produced by the `stringAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitStringAtomExp?: (ctx: StringAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `regexExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitRegexExp?: (ctx: RegexExpContext) => Result;
    /**
     * Visit a parse tree produced by the `powerExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitPowerExp?: (ctx: PowerExpContext) => Result;
    /**
     * Visit a parse tree produced by the `comparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitComparisonExp?: (ctx: ComparisonExpContext) => Result;
    /**
     * Visit a parse tree produced by the `functionCall`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFunctionCall?: (ctx: FunctionCallContext) => Result;
    /**
     * Visit a parse tree produced by the `hexNumberAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitHexNumberAtomExp?: (ctx: HexNumberAtomExpContext) => Result;
    /**
     * Visit a parse tree produced by the `parenthesisExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitParenthesisExp?: (ctx: ParenthesisExpContext) => Result;
    /**
     * Visit a parse tree produced by `UBExprParser.expressionList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitExpressionList?: (ctx: ExpressionListContext) => Result;
    /**
     * Visit a parse tree produced by `UBExprParser.functionBody`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFunctionBody?: (ctx: FunctionBodyContext) => Result;
    /**
     * Visit a parse tree produced by `UBExprParser.userFunctionCallArgs`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitUserFunctionCallArgs?: (ctx: UserFunctionCallArgsContext) => Result;
    /**
     * Visit a parse tree produced by `UBExprParser.functionArgs`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitFunctionArgs?: (ctx: FunctionArgsContext) => Result;
}

