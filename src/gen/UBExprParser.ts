// Generated from ./UBExprParser.g4 by ANTLR 4.13.1

import * as antlr from "antlr4ng";
import { Token } from "antlr4ng";

import { UBExprParserListener } from "./UBExprParserListener.js";
import { UBExprParserVisitor } from "./UBExprParserVisitor.js";

// for running tests with parameters, TODO: discuss strategy for typed parameters in CI
// eslint-disable-next-line no-unused-vars
type int = number;


export class UBExprParser extends antlr.Parser {
    public static readonly OPBITSHIFTL = 1;
    public static readonly OPBITSHIFTR = 2;
    public static readonly OPAND = 3;
    public static readonly OPOR = 4;
    public static readonly FATARROW = 5;
    public static readonly LTEQ = 6;
    public static readonly GTEQ = 7;
    public static readonly EQEQ = 8;
    public static readonly NEQ = 9;
    public static readonly COMMA = 10;
    public static readonly SEMICOLON = 11;
    public static readonly COLON = 12;
    public static readonly DOLLAR = 13;
    public static readonly AT = 14;
    public static readonly AMPER = 15;
    public static readonly CARAT = 16;
    public static readonly BAR = 17;
    public static readonly TILDE = 18;
    public static readonly FSLASH = 19;
    public static readonly ASTERISK = 20;
    public static readonly PERCENT = 21;
    public static readonly POUND = 22;
    public static readonly PLUS = 23;
    public static readonly MINUS = 24;
    public static readonly GT = 25;
    public static readonly LT = 26;
    public static readonly EQ = 27;
    public static readonly EXCLAMATION = 28;
    public static readonly BACKTICK = 29;
    public static readonly OPENPAREN = 30;
    public static readonly CLOSEPAREN = 31;
    public static readonly OPENCURL = 32;
    public static readonly CLOSECURL = 33;
    public static readonly OPENSQUARE = 34;
    public static readonly CLOSESQUARE = 35;
    public static readonly BOOL = 36;
    public static readonly USTRING = 37;
    public static readonly STRING = 38;
    public static readonly HEX = 39;
    public static readonly NUMBER = 40;
    public static readonly WHITESPACE = 41;
    public static readonly COMMENT = 42;
    public static readonly ANY = 43;
    public static readonly RULE_root = 0;
    public static readonly RULE_expression = 1;
    public static readonly RULE_expressionList = 2;
    public static readonly RULE_functionBody = 3;
    public static readonly RULE_userFunctionCallArgs = 4;
    public static readonly RULE_functionArgs = 5;

    public static readonly literalNames = [
        null, "'<<'", "'>>'", "'&&'", "'||'", "'=>'", "'<='", "'>='", "'=='", 
        "'!='", "','", "';'", "':'", "'$'", "'@'", "'&'", "'^'", "'|'", 
        "'~'", "'/'", "'*'", "'%'", "'#'", "'+'", "'-'", "'>'", "'<'", "'='", 
        "'!'", "'`'", "'('", "')'", "'{'", "'}'", "'['", "']'"
    ];

    public static readonly symbolicNames = [
        null, "OPBITSHIFTL", "OPBITSHIFTR", "OPAND", "OPOR", "FATARROW", 
        "LTEQ", "GTEQ", "EQEQ", "NEQ", "COMMA", "SEMICOLON", "COLON", "DOLLAR", 
        "AT", "AMPER", "CARAT", "BAR", "TILDE", "FSLASH", "ASTERISK", "PERCENT", 
        "POUND", "PLUS", "MINUS", "GT", "LT", "EQ", "EXCLAMATION", "BACKTICK", 
        "OPENPAREN", "CLOSEPAREN", "OPENCURL", "CLOSECURL", "OPENSQUARE", 
        "CLOSESQUARE", "BOOL", "USTRING", "STRING", "HEX", "NUMBER", "WHITESPACE", 
        "COMMENT", "ANY"
    ];
    public static readonly ruleNames = [
        "root", "expression", "expressionList", "functionBody", "userFunctionCallArgs", 
        "functionArgs",
    ];

    public get grammarFileName(): string { return "UBExprParser.g4"; }
    public get literalNames(): (string | null)[] { return UBExprParser.literalNames; }
    public get symbolicNames(): (string | null)[] { return UBExprParser.symbolicNames; }
    public get ruleNames(): string[] { return UBExprParser.ruleNames; }
    public get serializedATN(): number[] { return UBExprParser._serializedATN; }

    protected createFailedPredicateException(predicate?: string, message?: string): antlr.FailedPredicateException {
        return new antlr.FailedPredicateException(this, predicate, message);
    }

    public constructor(input: antlr.TokenStream) {
        super(input);
        this.interpreter = new antlr.ParserATNSimulator(this, UBExprParser._ATN, UBExprParser.decisionsToDFA, new antlr.PredictionContextCache());
    }
    public root(): RootContext {
        let localContext = new RootContext(this.context, this.state);
        this.enterRule(localContext, 0, UBExprParser.RULE_root);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            {
            this.state = 15;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 0, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 12;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                }
                this.state = 17;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 0, this.context);
            }
            this.state = 18;
            this.expression(0);
            this.state = 35;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 3, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 22;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                    while (_la === 41) {
                        {
                        {
                        this.state = 19;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                        this.state = 24;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                    }
                    this.state = 25;
                    this.match(UBExprParser.SEMICOLON);
                    this.state = 29;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 2, this.context);
                    while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                        if (alternative === 1) {
                            {
                            {
                            this.state = 26;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                        }
                        this.state = 31;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 2, this.context);
                    }
                    this.state = 32;
                    this.expression(0);
                    }
                    }
                }
                this.state = 37;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 3, this.context);
            }
            this.state = 41;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 4, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 38;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                }
                this.state = 43;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 4, this.context);
            }
            this.state = 45;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 11) {
                {
                this.state = 44;
                this.match(UBExprParser.SEMICOLON);
                }
            }

            }
            this.state = 50;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 41) {
                {
                {
                this.state = 47;
                this.match(UBExprParser.WHITESPACE);
                }
                }
                this.state = 52;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 53;
            this.match(UBExprParser.EOF);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }

    public expression(): ExpressionContext;
    public expression(_p: number): ExpressionContext;
    public expression(_p?: number): ExpressionContext {
        if (_p === undefined) {
            _p = 0;
        }

        let parentContext = this.context;
        let parentState = this.state;
        let localContext = new ExpressionContext(this.context, parentState);
        let previousContext = localContext;
        let _startState = 2;
        this.enterRecursionRule(localContext, 2, UBExprParser.RULE_expression, _p);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 126;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 15, this.context) ) {
            case 1:
                {
                localContext = new InlineFunctionContext(localContext);
                this.context = localContext;
                previousContext = localContext;

                this.state = 56;
                this.match(UBExprParser.OPENPAREN);
                this.state = 57;
                this.functionArgs();
                this.state = 58;
                this.match(UBExprParser.CLOSEPAREN);
                this.state = 62;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 41) {
                    {
                    {
                    this.state = 59;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                    this.state = 64;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 65;
                this.match(UBExprParser.FATARROW);
                this.state = 69;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 41) {
                    {
                    {
                    this.state = 66;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                    this.state = 71;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 72;
                this.match(UBExprParser.OPENCURL);
                this.state = 75;
                this.errorHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this.tokenStream, 9, this.context) ) {
                case 1:
                    {
                    this.state = 73;
                    this.functionBody();
                    }
                    break;
                case 2:
                    // tslint:disable-next-line:no-empty
                    {
                    }
                    break;
                }
                this.state = 77;
                this.match(UBExprParser.CLOSECURL);
                }
                break;
            case 2:
                {
                localContext = new ParenthesisExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 79;
                this.match(UBExprParser.OPENPAREN);
                this.state = 83;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 10, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                        {
                        this.state = 80;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                    }
                    this.state = 85;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 10, this.context);
                }
                this.state = 86;
                this.expression(0);
                this.state = 90;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 41) {
                    {
                    {
                    this.state = 87;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                    this.state = 92;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 93;
                this.match(UBExprParser.CLOSEPAREN);
                }
                break;
            case 3:
                {
                localContext = new GetvarAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 95;
                (localContext as GetvarAtomExpContext)._id = this.tokenStream.LT(1);
                _la = this.tokenStream.LA(1);
                if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 57344) !== 0))) {
                    (localContext as GetvarAtomExpContext)._id = this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
                this.state = 96;
                this.expression(18);
                }
                break;
            case 4:
                {
                localContext = new NumericAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 98;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                if (_la === 24) {
                    {
                    this.state = 97;
                    this.match(UBExprParser.MINUS);
                    }
                }

                this.state = 100;
                this.match(UBExprParser.NUMBER);
                }
                break;
            case 5:
                {
                localContext = new FunctionCallContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 101;
                this.match(UBExprParser.USTRING);
                this.state = 102;
                this.expressionList();
                }
                break;
            case 6:
                {
                localContext = new BitwiseComplementOpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 103;
                this.match(UBExprParser.TILDE);
                this.state = 104;
                this.expression(14);
                }
                break;
            case 7:
                {
                localContext = new SetVarExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 105;
                (localContext as SetVarExpContext)._id = this.tokenStream.LT(1);
                _la = this.tokenStream.LA(1);
                if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 57344) !== 0))) {
                    (localContext as SetVarExpContext)._id = this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
                this.state = 106;
                this.expression(0);
                this.state = 110;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 41) {
                    {
                    {
                    this.state = 107;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                    this.state = 112;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 113;
                this.match(UBExprParser.EQ);
                this.state = 117;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 14, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                        {
                        this.state = 114;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                    }
                    this.state = 119;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 14, this.context);
                }
                this.state = 120;
                this.expression(7);
                }
                break;
            case 8:
                {
                localContext = new BoolAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 122;
                this.match(UBExprParser.BOOL);
                }
                break;
            case 9:
                {
                localContext = new StringAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 123;
                (localContext as StringAtomExpContext)._str = this.tokenStream.LT(1);
                _la = this.tokenStream.LA(1);
                if(!(_la === 37 || _la === 38)) {
                    (localContext as StringAtomExpContext)._str = this.errorHandler.recoverInline(this);
                }
                else {
                    this.errorHandler.reportMatch(this);
                    this.consume();
                }
                }
                break;
            case 10:
                {
                localContext = new HexNumberAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 124;
                this.match(UBExprParser.HEX);
                }
                break;
            case 11:
                {
                localContext = new CatchallAtomExpContext(localContext);
                this.context = localContext;
                previousContext = localContext;
                this.state = 125;
                this.matchWildcard();
                }
                break;
            }
            this.context!.stop = this.tokenStream.LT(-1);
            this.state = 260;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 33, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    if (this._parseListeners != null) {
                        this.triggerExitRuleEvent();
                    }
                    previousContext = localContext;
                    {
                    this.state = 258;
                    this.errorHandler.sync(this);
                    switch (this.interpreter.adaptivePredict(this.tokenStream, 32, this.context) ) {
                    case 1:
                        {
                        localContext = new BitshiftOpsContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 128;
                        if (!(this.precpred(this.context, 13))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 13)");
                        }
                        this.state = 132;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 129;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 134;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 135;
                        (localContext as BitshiftOpsContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!(_la === 1 || _la === 2)) {
                            (localContext as BitshiftOpsContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 139;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 17, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 136;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 141;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 17, this.context);
                        }
                        this.state = 142;
                        this.expression(14);
                        }
                        break;
                    case 2:
                        {
                        localContext = new BitwiseOpsContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 143;
                        if (!(this.precpred(this.context, 12))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 12)");
                        }
                        this.state = 147;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 144;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 149;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 150;
                        (localContext as BitwiseOpsContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 229376) !== 0))) {
                            (localContext as BitwiseOpsContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 154;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 19, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 151;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 156;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 19, this.context);
                        }
                        this.state = 157;
                        this.expression(13);
                        }
                        break;
                    case 3:
                        {
                        localContext = new PowerExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 158;
                        if (!(this.precpred(this.context, 11))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 11)");
                        }
                        this.state = 162;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 159;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 164;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 165;
                        this.match(UBExprParser.CARAT);
                        this.state = 169;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 21, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 166;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 171;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 21, this.context);
                        }
                        this.state = 172;
                        this.expression(11);
                        }
                        break;
                    case 4:
                        {
                        localContext = new MulDivExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 173;
                        if (!(this.precpred(this.context, 10))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 10)");
                        }
                        this.state = 177;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 174;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 179;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 180;
                        (localContext as MulDivExpContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 3670016) !== 0))) {
                            (localContext as MulDivExpContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 184;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 23, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 181;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 186;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 23, this.context);
                        }
                        this.state = 187;
                        this.expression(11);
                        }
                        break;
                    case 5:
                        {
                        localContext = new AddSubExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 188;
                        if (!(this.precpred(this.context, 9))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 9)");
                        }
                        this.state = 192;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 189;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 194;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 195;
                        (localContext as AddSubExpContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!(_la === 23 || _la === 24)) {
                            (localContext as AddSubExpContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 199;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 25, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 196;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 201;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 25, this.context);
                        }
                        this.state = 202;
                        this.expression(10);
                        }
                        break;
                    case 6:
                        {
                        localContext = new RegexExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 203;
                        if (!(this.precpred(this.context, 8))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 8)");
                        }
                        this.state = 204;
                        this.match(UBExprParser.POUND);
                        this.state = 205;
                        this.expression(9);
                        }
                        break;
                    case 7:
                        {
                        localContext = new ComparisonExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 206;
                        if (!(this.precpred(this.context, 6))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 6)");
                        }
                        this.state = 210;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 207;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 212;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 213;
                        (localContext as ComparisonExpContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 100664256) !== 0))) {
                            (localContext as ComparisonExpContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 217;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 27, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 214;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 219;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 27, this.context);
                        }
                        this.state = 220;
                        this.expression(7);
                        }
                        break;
                    case 8:
                        {
                        localContext = new BooleanComparisonExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 221;
                        if (!(this.precpred(this.context, 5))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 5)");
                        }
                        this.state = 225;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                        while (_la === 41) {
                            {
                            {
                            this.state = 222;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                            this.state = 227;
                            this.errorHandler.sync(this);
                            _la = this.tokenStream.LA(1);
                        }
                        this.state = 228;
                        (localContext as BooleanComparisonExpContext)._op = this.tokenStream.LT(1);
                        _la = this.tokenStream.LA(1);
                        if(!(_la === 3 || _la === 4)) {
                            (localContext as BooleanComparisonExpContext)._op = this.errorHandler.recoverInline(this);
                        }
                        else {
                            this.errorHandler.reportMatch(this);
                            this.consume();
                        }
                        this.state = 232;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 29, this.context);
                        while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                            if (alternative === 1) {
                                {
                                {
                                this.state = 229;
                                this.match(UBExprParser.WHITESPACE);
                                }
                                }
                            }
                            this.state = 234;
                            this.errorHandler.sync(this);
                            alternative = this.interpreter.adaptivePredict(this.tokenStream, 29, this.context);
                        }
                        this.state = 235;
                        this.expression(6);
                        }
                        break;
                    case 9:
                        {
                        localContext = new UserFunctionCallExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 236;
                        if (!(this.precpred(this.context, 20))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 20)");
                        }
                        this.state = 237;
                        this.match(UBExprParser.OPENPAREN);
                        this.state = 239;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 30, this.context) ) {
                        case 1:
                            {
                            this.state = 238;
                            this.userFunctionCallArgs();
                            }
                            break;
                        }
                        this.state = 241;
                        this.match(UBExprParser.CLOSEPAREN);
                        }
                        break;
                    case 10:
                        {
                        localContext = new GetindexAtomExpContext(new ExpressionContext(parentContext, parentState));
                        this.pushNewRecursionContext(localContext, _startState, UBExprParser.RULE_expression);
                        this.state = 242;
                        if (!(this.precpred(this.context, 17))) {
                            throw this.createFailedPredicateException("this.precpred(this.context, 17)");
                        }
                        {
                        this.state = 243;
                        this.match(UBExprParser.OPENCURL);
                        this.state = 255;
                        this.errorHandler.sync(this);
                        switch (this.interpreter.adaptivePredict(this.tokenStream, 31, this.context) ) {
                        case 1:
                            {
                            this.state = 244;
                            (localContext as GetindexAtomExpContext)._c = this.match(UBExprParser.COLON);
                            }
                            break;
                        case 2:
                            {
                            {
                            this.state = 245;
                            (localContext as GetindexAtomExpContext)._c = this.match(UBExprParser.COLON);
                            this.state = 246;
                            (localContext as GetindexAtomExpContext)._i2 = this.expression(0);
                            }
                            }
                            break;
                        case 3:
                            {
                            this.state = 247;
                            (localContext as GetindexAtomExpContext)._i1 = this.expression(0);
                            }
                            break;
                        case 4:
                            {
                            {
                            this.state = 248;
                            (localContext as GetindexAtomExpContext)._i1 = this.expression(0);
                            this.state = 249;
                            (localContext as GetindexAtomExpContext)._c = this.match(UBExprParser.COLON);
                            this.state = 250;
                            (localContext as GetindexAtomExpContext)._i2 = this.expression(0);
                            }
                            }
                            break;
                        case 5:
                            {
                            {
                            this.state = 252;
                            (localContext as GetindexAtomExpContext)._i1 = this.expression(0);
                            this.state = 253;
                            (localContext as GetindexAtomExpContext)._c = this.match(UBExprParser.COLON);
                            }
                            }
                            break;
                        }
                        this.state = 257;
                        this.match(UBExprParser.CLOSEPAREN);
                        }
                        }
                        break;
                    }
                    }
                }
                this.state = 262;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 33, this.context);
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.unrollRecursionContexts(parentContext);
        }
        return localContext;
    }
    public expressionList(): ExpressionListContext {
        let localContext = new ExpressionListContext(this.context, this.state);
        this.enterRule(localContext, 4, UBExprParser.RULE_expressionList);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 263;
            this.match(UBExprParser.OPENSQUARE);
            this.state = 267;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 34, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 264;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                }
                this.state = 269;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 34, this.context);
            }
            this.state = 290;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 38, this.context) ) {
            case 1:
                {
                this.state = 270;
                this.expression(0);
                this.state = 274;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 35, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                        {
                        this.state = 271;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                    }
                    this.state = 276;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 35, this.context);
                }
                this.state = 287;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 10) {
                    {
                    {
                    this.state = 277;
                    this.match(UBExprParser.COMMA);
                    this.state = 281;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 36, this.context);
                    while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                        if (alternative === 1) {
                            {
                            {
                            this.state = 278;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                        }
                        this.state = 283;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 36, this.context);
                    }
                    this.state = 284;
                    this.expression(0);
                    }
                    }
                    this.state = 289;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                }
                break;
            }
            this.state = 295;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 41) {
                {
                {
                this.state = 292;
                this.match(UBExprParser.WHITESPACE);
                }
                }
                this.state = 297;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 298;
            this.match(UBExprParser.CLOSESQUARE);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public functionBody(): FunctionBodyContext {
        let localContext = new FunctionBodyContext(this.context, this.state);
        this.enterRule(localContext, 6, UBExprParser.RULE_functionBody);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            {
            this.state = 303;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 40, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 300;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                }
                this.state = 305;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 40, this.context);
            }
            this.state = 306;
            this.expression(0);
            this.state = 323;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 43, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 310;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                    while (_la === 41) {
                        {
                        {
                        this.state = 307;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                        this.state = 312;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                    }
                    this.state = 313;
                    this.match(UBExprParser.SEMICOLON);
                    this.state = 317;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 42, this.context);
                    while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                        if (alternative === 1) {
                            {
                            {
                            this.state = 314;
                            this.match(UBExprParser.WHITESPACE);
                            }
                            }
                        }
                        this.state = 319;
                        this.errorHandler.sync(this);
                        alternative = this.interpreter.adaptivePredict(this.tokenStream, 42, this.context);
                    }
                    this.state = 320;
                    this.expression(0);
                    }
                    }
                }
                this.state = 325;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 43, this.context);
            }
            this.state = 329;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 44, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 326;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                }
                this.state = 331;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 44, this.context);
            }
            this.state = 333;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 11) {
                {
                this.state = 332;
                this.match(UBExprParser.SEMICOLON);
                }
            }

            }
            this.state = 338;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 41) {
                {
                {
                this.state = 335;
                this.match(UBExprParser.WHITESPACE);
                }
                }
                this.state = 340;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public userFunctionCallArgs(): UserFunctionCallArgsContext {
        let localContext = new UserFunctionCallArgsContext(this.context, this.state);
        this.enterRule(localContext, 8, UBExprParser.RULE_userFunctionCallArgs);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 341;
            this.expression(0);
            this.state = 345;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 41) {
                {
                {
                this.state = 342;
                this.match(UBExprParser.WHITESPACE);
                }
                }
                this.state = 347;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 358;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 10) {
                {
                {
                this.state = 348;
                this.match(UBExprParser.COMMA);
                this.state = 352;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 48, this.context);
                while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                    if (alternative === 1) {
                        {
                        {
                        this.state = 349;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                    }
                    this.state = 354;
                    this.errorHandler.sync(this);
                    alternative = this.interpreter.adaptivePredict(this.tokenStream, 48, this.context);
                }
                this.state = 355;
                this.expression(0);
                }
                }
                this.state = 360;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public functionArgs(): FunctionArgsContext {
        let localContext = new FunctionArgsContext(this.context, this.state);
        this.enterRule(localContext, 10, UBExprParser.RULE_functionArgs);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 381;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 37) {
                {
                this.state = 361;
                this.match(UBExprParser.USTRING);
                this.state = 365;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 41) {
                    {
                    {
                    this.state = 362;
                    this.match(UBExprParser.WHITESPACE);
                    }
                    }
                    this.state = 367;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 378;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 10) {
                    {
                    {
                    this.state = 368;
                    this.match(UBExprParser.COMMA);
                    this.state = 372;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                    while (_la === 41) {
                        {
                        {
                        this.state = 369;
                        this.match(UBExprParser.WHITESPACE);
                        }
                        }
                        this.state = 374;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                    }
                    this.state = 375;
                    this.match(UBExprParser.USTRING);
                    }
                    }
                    this.state = 380;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                }
            }

            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }

    public override sempred(localContext: antlr.RuleContext, ruleIndex: number, predIndex: number): boolean {
        switch (ruleIndex) {
        case 1:
            return this.expression_sempred(localContext as ExpressionContext, predIndex);
        }
        return true;
    }
    private expression_sempred(localContext: ExpressionContext, predIndex: number): boolean {
        switch (predIndex) {
        case 0:
            return this.precpred(this.context, 13);
        case 1:
            return this.precpred(this.context, 12);
        case 2:
            return this.precpred(this.context, 11);
        case 3:
            return this.precpred(this.context, 10);
        case 4:
            return this.precpred(this.context, 9);
        case 5:
            return this.precpred(this.context, 8);
        case 6:
            return this.precpred(this.context, 6);
        case 7:
            return this.precpred(this.context, 5);
        case 8:
            return this.precpred(this.context, 20);
        case 9:
            return this.precpred(this.context, 17);
        }
        return true;
    }

    public static readonly _serializedATN: number[] = [
        4,1,43,384,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,1,0,5,
        0,14,8,0,10,0,12,0,17,9,0,1,0,1,0,5,0,21,8,0,10,0,12,0,24,9,0,1,
        0,1,0,5,0,28,8,0,10,0,12,0,31,9,0,1,0,5,0,34,8,0,10,0,12,0,37,9,
        0,1,0,5,0,40,8,0,10,0,12,0,43,9,0,1,0,3,0,46,8,0,1,0,5,0,49,8,0,
        10,0,12,0,52,9,0,1,0,1,0,1,1,1,1,1,1,1,1,1,1,5,1,61,8,1,10,1,12,
        1,64,9,1,1,1,1,1,5,1,68,8,1,10,1,12,1,71,9,1,1,1,1,1,1,1,3,1,76,
        8,1,1,1,1,1,1,1,1,1,5,1,82,8,1,10,1,12,1,85,9,1,1,1,1,1,5,1,89,8,
        1,10,1,12,1,92,9,1,1,1,1,1,1,1,1,1,1,1,3,1,99,8,1,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,1,5,1,109,8,1,10,1,12,1,112,9,1,1,1,1,1,5,1,116,
        8,1,10,1,12,1,119,9,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,127,8,1,1,1,1,
        1,5,1,131,8,1,10,1,12,1,134,9,1,1,1,1,1,5,1,138,8,1,10,1,12,1,141,
        9,1,1,1,1,1,1,1,5,1,146,8,1,10,1,12,1,149,9,1,1,1,1,1,5,1,153,8,
        1,10,1,12,1,156,9,1,1,1,1,1,1,1,5,1,161,8,1,10,1,12,1,164,9,1,1,
        1,1,1,5,1,168,8,1,10,1,12,1,171,9,1,1,1,1,1,1,1,5,1,176,8,1,10,1,
        12,1,179,9,1,1,1,1,1,5,1,183,8,1,10,1,12,1,186,9,1,1,1,1,1,1,1,5,
        1,191,8,1,10,1,12,1,194,9,1,1,1,1,1,5,1,198,8,1,10,1,12,1,201,9,
        1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,209,8,1,10,1,12,1,212,9,1,1,1,1,1,
        5,1,216,8,1,10,1,12,1,219,9,1,1,1,1,1,1,1,5,1,224,8,1,10,1,12,1,
        227,9,1,1,1,1,1,5,1,231,8,1,10,1,12,1,234,9,1,1,1,1,1,1,1,1,1,3,
        1,240,8,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        1,3,1,256,8,1,1,1,5,1,259,8,1,10,1,12,1,262,9,1,1,2,1,2,5,2,266,
        8,2,10,2,12,2,269,9,2,1,2,1,2,5,2,273,8,2,10,2,12,2,276,9,2,1,2,
        1,2,5,2,280,8,2,10,2,12,2,283,9,2,1,2,5,2,286,8,2,10,2,12,2,289,
        9,2,3,2,291,8,2,1,2,5,2,294,8,2,10,2,12,2,297,9,2,1,2,1,2,1,3,5,
        3,302,8,3,10,3,12,3,305,9,3,1,3,1,3,5,3,309,8,3,10,3,12,3,312,9,
        3,1,3,1,3,5,3,316,8,3,10,3,12,3,319,9,3,1,3,5,3,322,8,3,10,3,12,
        3,325,9,3,1,3,5,3,328,8,3,10,3,12,3,331,9,3,1,3,3,3,334,8,3,1,3,
        5,3,337,8,3,10,3,12,3,340,9,3,1,4,1,4,5,4,344,8,4,10,4,12,4,347,
        9,4,1,4,1,4,5,4,351,8,4,10,4,12,4,354,9,4,1,4,5,4,357,8,4,10,4,12,
        4,360,9,4,1,5,1,5,5,5,364,8,5,10,5,12,5,367,9,5,1,5,1,5,5,5,371,
        8,5,10,5,12,5,374,9,5,1,5,5,5,377,8,5,10,5,12,5,380,9,5,3,5,382,
        8,5,1,5,0,1,2,6,0,2,4,6,8,10,0,8,1,0,13,15,1,0,37,38,1,0,1,2,1,0,
        15,17,1,0,19,21,1,0,23,24,2,0,6,9,25,26,1,0,3,4,451,0,15,1,0,0,0,
        2,126,1,0,0,0,4,263,1,0,0,0,6,303,1,0,0,0,8,341,1,0,0,0,10,381,1,
        0,0,0,12,14,5,41,0,0,13,12,1,0,0,0,14,17,1,0,0,0,15,13,1,0,0,0,15,
        16,1,0,0,0,16,18,1,0,0,0,17,15,1,0,0,0,18,35,3,2,1,0,19,21,5,41,
        0,0,20,19,1,0,0,0,21,24,1,0,0,0,22,20,1,0,0,0,22,23,1,0,0,0,23,25,
        1,0,0,0,24,22,1,0,0,0,25,29,5,11,0,0,26,28,5,41,0,0,27,26,1,0,0,
        0,28,31,1,0,0,0,29,27,1,0,0,0,29,30,1,0,0,0,30,32,1,0,0,0,31,29,
        1,0,0,0,32,34,3,2,1,0,33,22,1,0,0,0,34,37,1,0,0,0,35,33,1,0,0,0,
        35,36,1,0,0,0,36,41,1,0,0,0,37,35,1,0,0,0,38,40,5,41,0,0,39,38,1,
        0,0,0,40,43,1,0,0,0,41,39,1,0,0,0,41,42,1,0,0,0,42,45,1,0,0,0,43,
        41,1,0,0,0,44,46,5,11,0,0,45,44,1,0,0,0,45,46,1,0,0,0,46,50,1,0,
        0,0,47,49,5,41,0,0,48,47,1,0,0,0,49,52,1,0,0,0,50,48,1,0,0,0,50,
        51,1,0,0,0,51,53,1,0,0,0,52,50,1,0,0,0,53,54,5,0,0,1,54,1,1,0,0,
        0,55,56,6,1,-1,0,56,57,5,30,0,0,57,58,3,10,5,0,58,62,5,31,0,0,59,
        61,5,41,0,0,60,59,1,0,0,0,61,64,1,0,0,0,62,60,1,0,0,0,62,63,1,0,
        0,0,63,65,1,0,0,0,64,62,1,0,0,0,65,69,5,5,0,0,66,68,5,41,0,0,67,
        66,1,0,0,0,68,71,1,0,0,0,69,67,1,0,0,0,69,70,1,0,0,0,70,72,1,0,0,
        0,71,69,1,0,0,0,72,75,5,32,0,0,73,76,3,6,3,0,74,76,1,0,0,0,75,73,
        1,0,0,0,75,74,1,0,0,0,76,77,1,0,0,0,77,78,5,33,0,0,78,127,1,0,0,
        0,79,83,5,30,0,0,80,82,5,41,0,0,81,80,1,0,0,0,82,85,1,0,0,0,83,81,
        1,0,0,0,83,84,1,0,0,0,84,86,1,0,0,0,85,83,1,0,0,0,86,90,3,2,1,0,
        87,89,5,41,0,0,88,87,1,0,0,0,89,92,1,0,0,0,90,88,1,0,0,0,90,91,1,
        0,0,0,91,93,1,0,0,0,92,90,1,0,0,0,93,94,5,31,0,0,94,127,1,0,0,0,
        95,96,7,0,0,0,96,127,3,2,1,18,97,99,5,24,0,0,98,97,1,0,0,0,98,99,
        1,0,0,0,99,100,1,0,0,0,100,127,5,40,0,0,101,102,5,37,0,0,102,127,
        3,4,2,0,103,104,5,18,0,0,104,127,3,2,1,14,105,106,7,0,0,0,106,110,
        3,2,1,0,107,109,5,41,0,0,108,107,1,0,0,0,109,112,1,0,0,0,110,108,
        1,0,0,0,110,111,1,0,0,0,111,113,1,0,0,0,112,110,1,0,0,0,113,117,
        5,27,0,0,114,116,5,41,0,0,115,114,1,0,0,0,116,119,1,0,0,0,117,115,
        1,0,0,0,117,118,1,0,0,0,118,120,1,0,0,0,119,117,1,0,0,0,120,121,
        3,2,1,7,121,127,1,0,0,0,122,127,5,36,0,0,123,127,7,1,0,0,124,127,
        5,39,0,0,125,127,9,0,0,0,126,55,1,0,0,0,126,79,1,0,0,0,126,95,1,
        0,0,0,126,98,1,0,0,0,126,101,1,0,0,0,126,103,1,0,0,0,126,105,1,0,
        0,0,126,122,1,0,0,0,126,123,1,0,0,0,126,124,1,0,0,0,126,125,1,0,
        0,0,127,260,1,0,0,0,128,132,10,13,0,0,129,131,5,41,0,0,130,129,1,
        0,0,0,131,134,1,0,0,0,132,130,1,0,0,0,132,133,1,0,0,0,133,135,1,
        0,0,0,134,132,1,0,0,0,135,139,7,2,0,0,136,138,5,41,0,0,137,136,1,
        0,0,0,138,141,1,0,0,0,139,137,1,0,0,0,139,140,1,0,0,0,140,142,1,
        0,0,0,141,139,1,0,0,0,142,259,3,2,1,14,143,147,10,12,0,0,144,146,
        5,41,0,0,145,144,1,0,0,0,146,149,1,0,0,0,147,145,1,0,0,0,147,148,
        1,0,0,0,148,150,1,0,0,0,149,147,1,0,0,0,150,154,7,3,0,0,151,153,
        5,41,0,0,152,151,1,0,0,0,153,156,1,0,0,0,154,152,1,0,0,0,154,155,
        1,0,0,0,155,157,1,0,0,0,156,154,1,0,0,0,157,259,3,2,1,13,158,162,
        10,11,0,0,159,161,5,41,0,0,160,159,1,0,0,0,161,164,1,0,0,0,162,160,
        1,0,0,0,162,163,1,0,0,0,163,165,1,0,0,0,164,162,1,0,0,0,165,169,
        5,16,0,0,166,168,5,41,0,0,167,166,1,0,0,0,168,171,1,0,0,0,169,167,
        1,0,0,0,169,170,1,0,0,0,170,172,1,0,0,0,171,169,1,0,0,0,172,259,
        3,2,1,11,173,177,10,10,0,0,174,176,5,41,0,0,175,174,1,0,0,0,176,
        179,1,0,0,0,177,175,1,0,0,0,177,178,1,0,0,0,178,180,1,0,0,0,179,
        177,1,0,0,0,180,184,7,4,0,0,181,183,5,41,0,0,182,181,1,0,0,0,183,
        186,1,0,0,0,184,182,1,0,0,0,184,185,1,0,0,0,185,187,1,0,0,0,186,
        184,1,0,0,0,187,259,3,2,1,11,188,192,10,9,0,0,189,191,5,41,0,0,190,
        189,1,0,0,0,191,194,1,0,0,0,192,190,1,0,0,0,192,193,1,0,0,0,193,
        195,1,0,0,0,194,192,1,0,0,0,195,199,7,5,0,0,196,198,5,41,0,0,197,
        196,1,0,0,0,198,201,1,0,0,0,199,197,1,0,0,0,199,200,1,0,0,0,200,
        202,1,0,0,0,201,199,1,0,0,0,202,259,3,2,1,10,203,204,10,8,0,0,204,
        205,5,22,0,0,205,259,3,2,1,9,206,210,10,6,0,0,207,209,5,41,0,0,208,
        207,1,0,0,0,209,212,1,0,0,0,210,208,1,0,0,0,210,211,1,0,0,0,211,
        213,1,0,0,0,212,210,1,0,0,0,213,217,7,6,0,0,214,216,5,41,0,0,215,
        214,1,0,0,0,216,219,1,0,0,0,217,215,1,0,0,0,217,218,1,0,0,0,218,
        220,1,0,0,0,219,217,1,0,0,0,220,259,3,2,1,7,221,225,10,5,0,0,222,
        224,5,41,0,0,223,222,1,0,0,0,224,227,1,0,0,0,225,223,1,0,0,0,225,
        226,1,0,0,0,226,228,1,0,0,0,227,225,1,0,0,0,228,232,7,7,0,0,229,
        231,5,41,0,0,230,229,1,0,0,0,231,234,1,0,0,0,232,230,1,0,0,0,232,
        233,1,0,0,0,233,235,1,0,0,0,234,232,1,0,0,0,235,259,3,2,1,6,236,
        237,10,20,0,0,237,239,5,30,0,0,238,240,3,8,4,0,239,238,1,0,0,0,239,
        240,1,0,0,0,240,241,1,0,0,0,241,259,5,31,0,0,242,243,10,17,0,0,243,
        255,5,32,0,0,244,256,5,12,0,0,245,246,5,12,0,0,246,256,3,2,1,0,247,
        256,3,2,1,0,248,249,3,2,1,0,249,250,5,12,0,0,250,251,3,2,1,0,251,
        256,1,0,0,0,252,253,3,2,1,0,253,254,5,12,0,0,254,256,1,0,0,0,255,
        244,1,0,0,0,255,245,1,0,0,0,255,247,1,0,0,0,255,248,1,0,0,0,255,
        252,1,0,0,0,256,257,1,0,0,0,257,259,5,31,0,0,258,128,1,0,0,0,258,
        143,1,0,0,0,258,158,1,0,0,0,258,173,1,0,0,0,258,188,1,0,0,0,258,
        203,1,0,0,0,258,206,1,0,0,0,258,221,1,0,0,0,258,236,1,0,0,0,258,
        242,1,0,0,0,259,262,1,0,0,0,260,258,1,0,0,0,260,261,1,0,0,0,261,
        3,1,0,0,0,262,260,1,0,0,0,263,267,5,34,0,0,264,266,5,41,0,0,265,
        264,1,0,0,0,266,269,1,0,0,0,267,265,1,0,0,0,267,268,1,0,0,0,268,
        290,1,0,0,0,269,267,1,0,0,0,270,274,3,2,1,0,271,273,5,41,0,0,272,
        271,1,0,0,0,273,276,1,0,0,0,274,272,1,0,0,0,274,275,1,0,0,0,275,
        287,1,0,0,0,276,274,1,0,0,0,277,281,5,10,0,0,278,280,5,41,0,0,279,
        278,1,0,0,0,280,283,1,0,0,0,281,279,1,0,0,0,281,282,1,0,0,0,282,
        284,1,0,0,0,283,281,1,0,0,0,284,286,3,2,1,0,285,277,1,0,0,0,286,
        289,1,0,0,0,287,285,1,0,0,0,287,288,1,0,0,0,288,291,1,0,0,0,289,
        287,1,0,0,0,290,270,1,0,0,0,290,291,1,0,0,0,291,295,1,0,0,0,292,
        294,5,41,0,0,293,292,1,0,0,0,294,297,1,0,0,0,295,293,1,0,0,0,295,
        296,1,0,0,0,296,298,1,0,0,0,297,295,1,0,0,0,298,299,5,35,0,0,299,
        5,1,0,0,0,300,302,5,41,0,0,301,300,1,0,0,0,302,305,1,0,0,0,303,301,
        1,0,0,0,303,304,1,0,0,0,304,306,1,0,0,0,305,303,1,0,0,0,306,323,
        3,2,1,0,307,309,5,41,0,0,308,307,1,0,0,0,309,312,1,0,0,0,310,308,
        1,0,0,0,310,311,1,0,0,0,311,313,1,0,0,0,312,310,1,0,0,0,313,317,
        5,11,0,0,314,316,5,41,0,0,315,314,1,0,0,0,316,319,1,0,0,0,317,315,
        1,0,0,0,317,318,1,0,0,0,318,320,1,0,0,0,319,317,1,0,0,0,320,322,
        3,2,1,0,321,310,1,0,0,0,322,325,1,0,0,0,323,321,1,0,0,0,323,324,
        1,0,0,0,324,329,1,0,0,0,325,323,1,0,0,0,326,328,5,41,0,0,327,326,
        1,0,0,0,328,331,1,0,0,0,329,327,1,0,0,0,329,330,1,0,0,0,330,333,
        1,0,0,0,331,329,1,0,0,0,332,334,5,11,0,0,333,332,1,0,0,0,333,334,
        1,0,0,0,334,338,1,0,0,0,335,337,5,41,0,0,336,335,1,0,0,0,337,340,
        1,0,0,0,338,336,1,0,0,0,338,339,1,0,0,0,339,7,1,0,0,0,340,338,1,
        0,0,0,341,345,3,2,1,0,342,344,5,41,0,0,343,342,1,0,0,0,344,347,1,
        0,0,0,345,343,1,0,0,0,345,346,1,0,0,0,346,358,1,0,0,0,347,345,1,
        0,0,0,348,352,5,10,0,0,349,351,5,41,0,0,350,349,1,0,0,0,351,354,
        1,0,0,0,352,350,1,0,0,0,352,353,1,0,0,0,353,355,1,0,0,0,354,352,
        1,0,0,0,355,357,3,2,1,0,356,348,1,0,0,0,357,360,1,0,0,0,358,356,
        1,0,0,0,358,359,1,0,0,0,359,9,1,0,0,0,360,358,1,0,0,0,361,365,5,
        37,0,0,362,364,5,41,0,0,363,362,1,0,0,0,364,367,1,0,0,0,365,363,
        1,0,0,0,365,366,1,0,0,0,366,378,1,0,0,0,367,365,1,0,0,0,368,372,
        5,10,0,0,369,371,5,41,0,0,370,369,1,0,0,0,371,374,1,0,0,0,372,370,
        1,0,0,0,372,373,1,0,0,0,373,375,1,0,0,0,374,372,1,0,0,0,375,377,
        5,37,0,0,376,368,1,0,0,0,377,380,1,0,0,0,378,376,1,0,0,0,378,379,
        1,0,0,0,379,382,1,0,0,0,380,378,1,0,0,0,381,361,1,0,0,0,381,382,
        1,0,0,0,382,11,1,0,0,0,54,15,22,29,35,41,45,50,62,69,75,83,90,98,
        110,117,126,132,139,147,154,162,169,177,184,192,199,210,217,225,
        232,239,255,258,260,267,274,281,287,290,295,303,310,317,323,329,
        333,338,345,352,358,365,372,378,381
    ];

    private static __ATN: antlr.ATN;
    public static get _ATN(): antlr.ATN {
        if (!UBExprParser.__ATN) {
            UBExprParser.__ATN = new antlr.ATNDeserializer().deserialize(UBExprParser._serializedATN);
        }

        return UBExprParser.__ATN;
    }


    private static readonly vocabulary = new antlr.Vocabulary(UBExprParser.literalNames, UBExprParser.symbolicNames, []);

    public override get vocabulary(): antlr.Vocabulary {
        return UBExprParser.vocabulary;
    }

    private static readonly decisionsToDFA = UBExprParser._ATN.decisionToState.map( (ds: antlr.DecisionState, index: number) => new antlr.DFA(ds, index) );
}

export class RootContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public EOF(): antlr.TerminalNode {
        return this.getToken(UBExprParser.EOF, 0)!;
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public SEMICOLON(): antlr.TerminalNode[];
    public SEMICOLON(i: number): antlr.TerminalNode | null;
    public SEMICOLON(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.SEMICOLON);
    	} else {
    		return this.getToken(UBExprParser.SEMICOLON, i);
    	}
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_root;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterRoot) {
             listener.enterRoot(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitRoot) {
             listener.exitRoot(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitRoot) {
            return visitor.visitRoot(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ExpressionContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_expression;
    }
    public override copyFrom(ctx: ExpressionContext): void {
        super.copyFrom(ctx);
    }
}
export class BitshiftOpsContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public OPBITSHIFTR(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.OPBITSHIFTR, 0);
    }
    public OPBITSHIFTL(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.OPBITSHIFTL, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterBitshiftOps) {
             listener.enterBitshiftOps(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitBitshiftOps) {
             listener.exitBitshiftOps(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitBitshiftOps) {
            return visitor.visitBitshiftOps(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class NumericAtomExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public NUMBER(): antlr.TerminalNode {
        return this.getToken(UBExprParser.NUMBER, 0)!;
    }
    public MINUS(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.MINUS, 0);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterNumericAtomExp) {
             listener.enterNumericAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitNumericAtomExp) {
             listener.exitNumericAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitNumericAtomExp) {
            return visitor.visitNumericAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class CatchallAtomExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterCatchallAtomExp) {
             listener.enterCatchallAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitCatchallAtomExp) {
             listener.exitCatchallAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitCatchallAtomExp) {
            return visitor.visitCatchallAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class BitwiseComplementOpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public TILDE(): antlr.TerminalNode {
        return this.getToken(UBExprParser.TILDE, 0)!;
    }
    public expression(): ExpressionContext {
        return this.getRuleContext(0, ExpressionContext)!;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterBitwiseComplementOp) {
             listener.enterBitwiseComplementOp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitBitwiseComplementOp) {
             listener.exitBitwiseComplementOp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitBitwiseComplementOp) {
            return visitor.visitBitwiseComplementOp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class MulDivExpContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public ASTERISK(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.ASTERISK, 0);
    }
    public FSLASH(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.FSLASH, 0);
    }
    public PERCENT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.PERCENT, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterMulDivExp) {
             listener.enterMulDivExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitMulDivExp) {
             listener.exitMulDivExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitMulDivExp) {
            return visitor.visitMulDivExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class BoolAtomExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public BOOL(): antlr.TerminalNode {
        return this.getToken(UBExprParser.BOOL, 0)!;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterBoolAtomExp) {
             listener.enterBoolAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitBoolAtomExp) {
             listener.exitBoolAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitBoolAtomExp) {
            return visitor.visitBoolAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class GetvarAtomExpContext extends ExpressionContext {
    public _id: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext {
        return this.getRuleContext(0, ExpressionContext)!;
    }
    public DOLLAR(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.DOLLAR, 0);
    }
    public AT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.AT, 0);
    }
    public AMPER(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.AMPER, 0);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterGetvarAtomExp) {
             listener.enterGetvarAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitGetvarAtomExp) {
             listener.exitGetvarAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitGetvarAtomExp) {
            return visitor.visitGetvarAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class InlineFunctionContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public OPENPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.OPENPAREN, 0)!;
    }
    public functionArgs(): FunctionArgsContext {
        return this.getRuleContext(0, FunctionArgsContext)!;
    }
    public CLOSEPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CLOSEPAREN, 0)!;
    }
    public FATARROW(): antlr.TerminalNode {
        return this.getToken(UBExprParser.FATARROW, 0)!;
    }
    public OPENCURL(): antlr.TerminalNode {
        return this.getToken(UBExprParser.OPENCURL, 0)!;
    }
    public CLOSECURL(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CLOSECURL, 0)!;
    }
    public functionBody(): FunctionBodyContext | null {
        return this.getRuleContext(0, FunctionBodyContext);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterInlineFunction) {
             listener.enterInlineFunction(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitInlineFunction) {
             listener.exitInlineFunction(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitInlineFunction) {
            return visitor.visitInlineFunction(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class UserFunctionCallExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext {
        return this.getRuleContext(0, ExpressionContext)!;
    }
    public OPENPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.OPENPAREN, 0)!;
    }
    public CLOSEPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CLOSEPAREN, 0)!;
    }
    public userFunctionCallArgs(): UserFunctionCallArgsContext | null {
        return this.getRuleContext(0, UserFunctionCallArgsContext);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterUserFunctionCallExp) {
             listener.enterUserFunctionCallExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitUserFunctionCallExp) {
             listener.exitUserFunctionCallExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitUserFunctionCallExp) {
            return visitor.visitUserFunctionCallExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class SetVarExpContext extends ExpressionContext {
    public _id: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public EQ(): antlr.TerminalNode {
        return this.getToken(UBExprParser.EQ, 0)!;
    }
    public DOLLAR(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.DOLLAR, 0);
    }
    public AT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.AT, 0);
    }
    public AMPER(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.AMPER, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterSetVarExp) {
             listener.enterSetVarExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitSetVarExp) {
             listener.exitSetVarExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitSetVarExp) {
            return visitor.visitSetVarExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class GetindexAtomExpContext extends ExpressionContext {
    public _c: Token | null;
    public _i2: ExpressionContext;
    public _i1: ExpressionContext;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public OPENCURL(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.OPENCURL, 0);
    }
    public CLOSEPAREN(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.CLOSEPAREN, 0);
    }
    public COLON(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.COLON, 0);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterGetindexAtomExp) {
             listener.enterGetindexAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitGetindexAtomExp) {
             listener.exitGetindexAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitGetindexAtomExp) {
            return visitor.visitGetindexAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class BitwiseOpsContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public AMPER(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.AMPER, 0);
    }
    public CARAT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.CARAT, 0);
    }
    public BAR(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.BAR, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterBitwiseOps) {
             listener.enterBitwiseOps(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitBitwiseOps) {
             listener.exitBitwiseOps(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitBitwiseOps) {
            return visitor.visitBitwiseOps(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class AddSubExpContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public PLUS(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.PLUS, 0);
    }
    public MINUS(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.MINUS, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterAddSubExp) {
             listener.enterAddSubExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitAddSubExp) {
             listener.exitAddSubExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitAddSubExp) {
            return visitor.visitAddSubExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class BooleanComparisonExpContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public OPAND(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.OPAND, 0);
    }
    public OPOR(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.OPOR, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterBooleanComparisonExp) {
             listener.enterBooleanComparisonExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitBooleanComparisonExp) {
             listener.exitBooleanComparisonExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitBooleanComparisonExp) {
            return visitor.visitBooleanComparisonExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class StringAtomExpContext extends ExpressionContext {
    public _str: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public STRING(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.STRING, 0);
    }
    public USTRING(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.USTRING, 0);
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterStringAtomExp) {
             listener.enterStringAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitStringAtomExp) {
             listener.exitStringAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitStringAtomExp) {
            return visitor.visitStringAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class RegexExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public POUND(): antlr.TerminalNode {
        return this.getToken(UBExprParser.POUND, 0)!;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterRegexExp) {
             listener.enterRegexExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitRegexExp) {
             listener.exitRegexExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitRegexExp) {
            return visitor.visitRegexExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class PowerExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public CARAT(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CARAT, 0)!;
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterPowerExp) {
             listener.enterPowerExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitPowerExp) {
             listener.exitPowerExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitPowerExp) {
            return visitor.visitPowerExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class ComparisonExpContext extends ExpressionContext {
    public _op: Token | null;
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public GT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.GT, 0);
    }
    public LT(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.LT, 0);
    }
    public GTEQ(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.GTEQ, 0);
    }
    public LTEQ(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.LTEQ, 0);
    }
    public EQEQ(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.EQEQ, 0);
    }
    public NEQ(): antlr.TerminalNode | null {
        return this.getToken(UBExprParser.NEQ, 0);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterComparisonExp) {
             listener.enterComparisonExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitComparisonExp) {
             listener.exitComparisonExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitComparisonExp) {
            return visitor.visitComparisonExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class FunctionCallContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public USTRING(): antlr.TerminalNode {
        return this.getToken(UBExprParser.USTRING, 0)!;
    }
    public expressionList(): ExpressionListContext {
        return this.getRuleContext(0, ExpressionListContext)!;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterFunctionCall) {
             listener.enterFunctionCall(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitFunctionCall) {
             listener.exitFunctionCall(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitFunctionCall) {
            return visitor.visitFunctionCall(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class HexNumberAtomExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public HEX(): antlr.TerminalNode {
        return this.getToken(UBExprParser.HEX, 0)!;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterHexNumberAtomExp) {
             listener.enterHexNumberAtomExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitHexNumberAtomExp) {
             listener.exitHexNumberAtomExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitHexNumberAtomExp) {
            return visitor.visitHexNumberAtomExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
export class ParenthesisExpContext extends ExpressionContext {
    public constructor(ctx: ExpressionContext) {
        super(ctx.parent, ctx.invokingState);
        super.copyFrom(ctx);
    }
    public OPENPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.OPENPAREN, 0)!;
    }
    public expression(): ExpressionContext {
        return this.getRuleContext(0, ExpressionContext)!;
    }
    public CLOSEPAREN(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CLOSEPAREN, 0)!;
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterParenthesisExp) {
             listener.enterParenthesisExp(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitParenthesisExp) {
             listener.exitParenthesisExp(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitParenthesisExp) {
            return visitor.visitParenthesisExp(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ExpressionListContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public OPENSQUARE(): antlr.TerminalNode {
        return this.getToken(UBExprParser.OPENSQUARE, 0)!;
    }
    public CLOSESQUARE(): antlr.TerminalNode {
        return this.getToken(UBExprParser.CLOSESQUARE, 0)!;
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public COMMA(): antlr.TerminalNode[];
    public COMMA(i: number): antlr.TerminalNode | null;
    public COMMA(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.COMMA);
    	} else {
    		return this.getToken(UBExprParser.COMMA, i);
    	}
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_expressionList;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterExpressionList) {
             listener.enterExpressionList(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitExpressionList) {
             listener.exitExpressionList(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitExpressionList) {
            return visitor.visitExpressionList(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class FunctionBodyContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public SEMICOLON(): antlr.TerminalNode[];
    public SEMICOLON(i: number): antlr.TerminalNode | null;
    public SEMICOLON(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.SEMICOLON);
    	} else {
    		return this.getToken(UBExprParser.SEMICOLON, i);
    	}
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_functionBody;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterFunctionBody) {
             listener.enterFunctionBody(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitFunctionBody) {
             listener.exitFunctionBody(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitFunctionBody) {
            return visitor.visitFunctionBody(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class UserFunctionCallArgsContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public expression(): ExpressionContext[];
    public expression(i: number): ExpressionContext | null;
    public expression(i?: number): ExpressionContext[] | ExpressionContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ExpressionContext);
        }

        return this.getRuleContext(i, ExpressionContext);
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public COMMA(): antlr.TerminalNode[];
    public COMMA(i: number): antlr.TerminalNode | null;
    public COMMA(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.COMMA);
    	} else {
    		return this.getToken(UBExprParser.COMMA, i);
    	}
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_userFunctionCallArgs;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterUserFunctionCallArgs) {
             listener.enterUserFunctionCallArgs(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitUserFunctionCallArgs) {
             listener.exitUserFunctionCallArgs(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitUserFunctionCallArgs) {
            return visitor.visitUserFunctionCallArgs(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class FunctionArgsContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public USTRING(): antlr.TerminalNode[];
    public USTRING(i: number): antlr.TerminalNode | null;
    public USTRING(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.USTRING);
    	} else {
    		return this.getToken(UBExprParser.USTRING, i);
    	}
    }
    public WHITESPACE(): antlr.TerminalNode[];
    public WHITESPACE(i: number): antlr.TerminalNode | null;
    public WHITESPACE(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.WHITESPACE);
    	} else {
    		return this.getToken(UBExprParser.WHITESPACE, i);
    	}
    }
    public COMMA(): antlr.TerminalNode[];
    public COMMA(i: number): antlr.TerminalNode | null;
    public COMMA(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(UBExprParser.COMMA);
    	} else {
    		return this.getToken(UBExprParser.COMMA, i);
    	}
    }
    public override get ruleIndex(): number {
        return UBExprParser.RULE_functionArgs;
    }
    public override enterRule(listener: UBExprParserListener): void {
        if(listener.enterFunctionArgs) {
             listener.enterFunctionArgs(this);
        }
    }
    public override exitRule(listener: UBExprParserListener): void {
        if(listener.exitFunctionArgs) {
             listener.exitFunctionArgs(this);
        }
    }
    public override accept<Result>(visitor: UBExprParserVisitor<Result>): Result | null {
        if (visitor.visitFunctionArgs) {
            return visitor.visitFunctionArgs(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
