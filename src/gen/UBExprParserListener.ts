// Generated from ./UBExprParser.g4 by ANTLR 4.13.1

import { ErrorNode, ParseTreeListener, ParserRuleContext, TerminalNode } from "antlr4ng";


import { RootContext } from "./UBExprParser.js";
import { BitshiftOpsContext } from "./UBExprParser.js";
import { NumericAtomExpContext } from "./UBExprParser.js";
import { CatchallAtomExpContext } from "./UBExprParser.js";
import { BitwiseComplementOpContext } from "./UBExprParser.js";
import { MulDivExpContext } from "./UBExprParser.js";
import { BoolAtomExpContext } from "./UBExprParser.js";
import { GetvarAtomExpContext } from "./UBExprParser.js";
import { InlineFunctionContext } from "./UBExprParser.js";
import { UserFunctionCallExpContext } from "./UBExprParser.js";
import { SetVarExpContext } from "./UBExprParser.js";
import { GetindexAtomExpContext } from "./UBExprParser.js";
import { BitwiseOpsContext } from "./UBExprParser.js";
import { AddSubExpContext } from "./UBExprParser.js";
import { BooleanComparisonExpContext } from "./UBExprParser.js";
import { StringAtomExpContext } from "./UBExprParser.js";
import { RegexExpContext } from "./UBExprParser.js";
import { PowerExpContext } from "./UBExprParser.js";
import { ComparisonExpContext } from "./UBExprParser.js";
import { FunctionCallContext } from "./UBExprParser.js";
import { HexNumberAtomExpContext } from "./UBExprParser.js";
import { ParenthesisExpContext } from "./UBExprParser.js";
import { ExpressionListContext } from "./UBExprParser.js";
import { FunctionBodyContext } from "./UBExprParser.js";
import { UserFunctionCallArgsContext } from "./UBExprParser.js";
import { FunctionArgsContext } from "./UBExprParser.js";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `UBExprParser`.
 */
export class UBExprParserListener implements ParseTreeListener {
    /**
     * Enter a parse tree produced by `UBExprParser.root`.
     * @param ctx the parse tree
     */
    enterRoot?: (ctx: RootContext) => void;
    /**
     * Exit a parse tree produced by `UBExprParser.root`.
     * @param ctx the parse tree
     */
    exitRoot?: (ctx: RootContext) => void;
    /**
     * Enter a parse tree produced by the `bitshiftOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterBitshiftOps?: (ctx: BitshiftOpsContext) => void;
    /**
     * Exit a parse tree produced by the `bitshiftOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitBitshiftOps?: (ctx: BitshiftOpsContext) => void;
    /**
     * Enter a parse tree produced by the `numericAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterNumericAtomExp?: (ctx: NumericAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `numericAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitNumericAtomExp?: (ctx: NumericAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `catchallAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterCatchallAtomExp?: (ctx: CatchallAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `catchallAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitCatchallAtomExp?: (ctx: CatchallAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `bitwiseComplementOp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterBitwiseComplementOp?: (ctx: BitwiseComplementOpContext) => void;
    /**
     * Exit a parse tree produced by the `bitwiseComplementOp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitBitwiseComplementOp?: (ctx: BitwiseComplementOpContext) => void;
    /**
     * Enter a parse tree produced by the `mulDivExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterMulDivExp?: (ctx: MulDivExpContext) => void;
    /**
     * Exit a parse tree produced by the `mulDivExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitMulDivExp?: (ctx: MulDivExpContext) => void;
    /**
     * Enter a parse tree produced by the `boolAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterBoolAtomExp?: (ctx: BoolAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `boolAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitBoolAtomExp?: (ctx: BoolAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `getvarAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterGetvarAtomExp?: (ctx: GetvarAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `getvarAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitGetvarAtomExp?: (ctx: GetvarAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `inlineFunction`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterInlineFunction?: (ctx: InlineFunctionContext) => void;
    /**
     * Exit a parse tree produced by the `inlineFunction`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitInlineFunction?: (ctx: InlineFunctionContext) => void;
    /**
     * Enter a parse tree produced by the `userFunctionCallExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterUserFunctionCallExp?: (ctx: UserFunctionCallExpContext) => void;
    /**
     * Exit a parse tree produced by the `userFunctionCallExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitUserFunctionCallExp?: (ctx: UserFunctionCallExpContext) => void;
    /**
     * Enter a parse tree produced by the `setVarExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterSetVarExp?: (ctx: SetVarExpContext) => void;
    /**
     * Exit a parse tree produced by the `setVarExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitSetVarExp?: (ctx: SetVarExpContext) => void;
    /**
     * Enter a parse tree produced by the `getindexAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterGetindexAtomExp?: (ctx: GetindexAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `getindexAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitGetindexAtomExp?: (ctx: GetindexAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `bitwiseOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterBitwiseOps?: (ctx: BitwiseOpsContext) => void;
    /**
     * Exit a parse tree produced by the `bitwiseOps`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitBitwiseOps?: (ctx: BitwiseOpsContext) => void;
    /**
     * Enter a parse tree produced by the `addSubExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterAddSubExp?: (ctx: AddSubExpContext) => void;
    /**
     * Exit a parse tree produced by the `addSubExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitAddSubExp?: (ctx: AddSubExpContext) => void;
    /**
     * Enter a parse tree produced by the `booleanComparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterBooleanComparisonExp?: (ctx: BooleanComparisonExpContext) => void;
    /**
     * Exit a parse tree produced by the `booleanComparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitBooleanComparisonExp?: (ctx: BooleanComparisonExpContext) => void;
    /**
     * Enter a parse tree produced by the `stringAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterStringAtomExp?: (ctx: StringAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `stringAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitStringAtomExp?: (ctx: StringAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `regexExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterRegexExp?: (ctx: RegexExpContext) => void;
    /**
     * Exit a parse tree produced by the `regexExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitRegexExp?: (ctx: RegexExpContext) => void;
    /**
     * Enter a parse tree produced by the `powerExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterPowerExp?: (ctx: PowerExpContext) => void;
    /**
     * Exit a parse tree produced by the `powerExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitPowerExp?: (ctx: PowerExpContext) => void;
    /**
     * Enter a parse tree produced by the `comparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterComparisonExp?: (ctx: ComparisonExpContext) => void;
    /**
     * Exit a parse tree produced by the `comparisonExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitComparisonExp?: (ctx: ComparisonExpContext) => void;
    /**
     * Enter a parse tree produced by the `functionCall`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterFunctionCall?: (ctx: FunctionCallContext) => void;
    /**
     * Exit a parse tree produced by the `functionCall`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitFunctionCall?: (ctx: FunctionCallContext) => void;
    /**
     * Enter a parse tree produced by the `hexNumberAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterHexNumberAtomExp?: (ctx: HexNumberAtomExpContext) => void;
    /**
     * Exit a parse tree produced by the `hexNumberAtomExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitHexNumberAtomExp?: (ctx: HexNumberAtomExpContext) => void;
    /**
     * Enter a parse tree produced by the `parenthesisExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    enterParenthesisExp?: (ctx: ParenthesisExpContext) => void;
    /**
     * Exit a parse tree produced by the `parenthesisExp`
     * labeled alternative in `UBExprParser.expression`.
     * @param ctx the parse tree
     */
    exitParenthesisExp?: (ctx: ParenthesisExpContext) => void;
    /**
     * Enter a parse tree produced by `UBExprParser.expressionList`.
     * @param ctx the parse tree
     */
    enterExpressionList?: (ctx: ExpressionListContext) => void;
    /**
     * Exit a parse tree produced by `UBExprParser.expressionList`.
     * @param ctx the parse tree
     */
    exitExpressionList?: (ctx: ExpressionListContext) => void;
    /**
     * Enter a parse tree produced by `UBExprParser.functionBody`.
     * @param ctx the parse tree
     */
    enterFunctionBody?: (ctx: FunctionBodyContext) => void;
    /**
     * Exit a parse tree produced by `UBExprParser.functionBody`.
     * @param ctx the parse tree
     */
    exitFunctionBody?: (ctx: FunctionBodyContext) => void;
    /**
     * Enter a parse tree produced by `UBExprParser.userFunctionCallArgs`.
     * @param ctx the parse tree
     */
    enterUserFunctionCallArgs?: (ctx: UserFunctionCallArgsContext) => void;
    /**
     * Exit a parse tree produced by `UBExprParser.userFunctionCallArgs`.
     * @param ctx the parse tree
     */
    exitUserFunctionCallArgs?: (ctx: UserFunctionCallArgsContext) => void;
    /**
     * Enter a parse tree produced by `UBExprParser.functionArgs`.
     * @param ctx the parse tree
     */
    enterFunctionArgs?: (ctx: FunctionArgsContext) => void;
    /**
     * Exit a parse tree produced by `UBExprParser.functionArgs`.
     * @param ctx the parse tree
     */
    exitFunctionArgs?: (ctx: FunctionArgsContext) => void;

    visitTerminal(node: TerminalNode): void {}
    visitErrorNode(node: ErrorNode): void {}
    enterEveryRule(node: ParserRuleContext): void {}
    exitEveryRule(node: ParserRuleContext): void {}
}

