# UtilityBelt Expression Parser

ubexpr-parser-ts

## Installation

`npm install ubexpr-parser-ts`

## Example


## Building release

```
npm run build
```

## Contributions

Contributions are welcome! If you find any issues, have suggestions, or want to improve the project, feel free to submit a pull request or open an issue on the GitLab repository.


## License

This project is licensed under the MIT License.
