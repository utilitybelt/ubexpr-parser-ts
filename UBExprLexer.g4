lexer grammar UBExprLexer;

fragment DIGIT: [0-9] ;

OPBITSHIFTL: '<<' ;
OPBITSHIFTR: '>>' ;
OPAND: '&&' ;
OPOR: '||' ;
FATARROW: '=>' ;
LTEQ: '<=' ;
GTEQ: '>=' ;
EQEQ: '==' ;
NEQ: '!=' ;

COMMA: ',' ;
SEMICOLON: ';' ;
COLON: ':' ;
DOLLAR: '$' ;
AT: '@' ;
AMPER: '&' ;
CARAT: '^' ;
BAR: '|' ;
TILDE: '~' ;
FSLASH: '/' ;
ASTERISK: '*' ;
PERCENT: '%' ;
POUND: '#' ;
PLUS: '+'  ;
MINUS: '-' ;
GT: '>' ;
LT: '<' ;
EQ: '=' ;
EXCLAMATION: '!' ;

BACKTICK: '`' ;
OPENPAREN: '(' ;
CLOSEPAREN: ')' ;
OPENCURL: '{' ;
CLOSECURL: '}' ;
OPENSQUARE: '[' ;
CLOSESQUARE: ']' ;

BOOL: ([tT][rR][uU][eE] | [fF][aA][lL][sS][eE]) ;

fragment BSLASH: '\\' ;
fragment STRCHAR: [a-zA-Z_"'] ;
fragment NEWLINE: '\r'? '\n' ;

USTRING: (STRCHAR | BSLASH .) (STRCHAR | ' ' | [0-9] | BSLASH .)*;
STRING: BACKTICK (BSLASH '`' | ~[`])* BACKTICK ;
HEX: '0x' [a-fA-F0-9]+ ;
NUMBER: MINUS? DIGIT+ ('.' DIGIT+)? ('E' ('+' | '-') [0-9]+)? ;

WHITESPACE: (' ' | '\t' | NEWLINE)+;

COMMENT: ((NEWLINE? WHITESPACE* '~~' ~('\n')*) | ('/~' .*? '~/')) -> channel(HIDDEN);
ANY: . ;